# Releases

## Versioning

This project does not follow the semver format. Instead it only has major and minor in its format (ex: `v1.0`). This is partially due to
how closed tied the operator is to the [Kubernetes api versions](https://kubernetes.io/docs/concepts/overview/kubernetes-api/#api-versioning)
which only specifies a major version and how stable it is. (ie `v1` or `v1beta1`). The CRD used by this operator is api versioned
as `v1beta1`, but we consider it to be in more of an alpha state. So we are using the version `v0.x`, where we change
the minor version for any operator changes.

When we are satisfied with the stability of the operator, we will move the version to `v1.x` (and `v1` for the api)

### CRD Versioning

Kubernetes has a page explaining [CRD Versioning](https://kubernetes.io/docs/tasks/access-kubernetes-api/custom-resources/custom-resource-definition-versioning/),
but we are currently not following their approach. This is due to:

- The `versions` field not being introduced until Kubernetes 1.11 (we still support 1.9)
- The fact that we are in more of an 'alpha/experimental' state with this operator, rather than beta or stable

We will do more with this in the future, and sync our major operator version with our CRD version.

### Relation to other GitLab versions

This operator is used to rollout GitLab updates. Each version of the operator is expected to be able to handle multiple
different versions of GitLab, and uses a different version scheme as a result.

The operator is more closely tied to specific versions of the [GitLab Chart](https://gitlab.com/charts/gitlab), as CRD
changes need to be reflected both here in this repo, and in the chart repo. But the main versioning concern we have for
the operator is the CRD, to ensure the same CRD contract is supported for both the chart and the operator. CRD versioning
does not follow a semver pattern, and as a result we don't sync our Operator version with the chart version (which is using semver)
See the [CRD Versioning](#crd-versioning) section for more details.

## Branches and Tags

- A `master` branch,
- Development branches targeting master
- `vx.x` tags from commits in master

When we want to include changes from master in our [GitLab Chart](https://gitlab.com/charts/gitlab), we will tag the commit.

## Releasing the operator

To release a new version of the operator, first bump the version in the `VERSION` file at the root of the repo. Once that
has been merged to the master branch, tag the commit with the version using `vx.x` syntax. This will cause CI to build and push
the tagged release image to the registry.
