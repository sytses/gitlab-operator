# GitLab Chart Kubernetes Operator

> **Note**: This operator is being used as an introduction to the operator pattern for the team working on the GitLab Chart,
> and may not represent a clean example of the operator pattern. See the [resources](#resources) for better starting points
> if you are looking to build your own operator.

## Summary

This GitLab Operator is responsible for watching for GitLab version changes in the [GitLab Chart](https://gitlab.com/charts/gitlab),
and running the appropriate migration and restart steps required to rollout the
new GitLab version without introducing downtime.

### Rollout behaviour

During a Reconcile run, the GitLab Operator attempts to take the following steps to rollout new GitLab versions:

1. Pause existing deployments.
2. Runs the shared secrets job, which generates secrets used as the defaults for service passwords and certificates.
3. Run GitLab migrations job, excluding post-migrations.
   - Post deployment migrations can perform migrations that mutate state that the running version of GitLab depends on.
     As a result we wait to run them until the new version is rolled out.
4. Restart the Gitaly StatefulSet Pods (if Gitaly is deployed in the cluster).
5. Unpause deployments to wait for the new GitLab version to be rolled out to the Pods.
6. Run the GitLab post-migrations job.
7. Perform a rolling update of the GitLab rails pods to sync the new database schema.

## Operator Components

The controller-runtime package is used for several of the components including the
Manager, Client and Clientsets, Listers, Sources and Informers. Documentation on
these components can be found in the [controller-runtime package](https://godoc.org/github.com/kubernetes-sigs/controller-runtime/pkg)

- [API Object Types](https://github.com/kubernetes/community/blob/master/contributors/devel/api-conventions.md#types-kinds)
  * Define the structure of the new resource type
- [CustomResourceDefinition](https://kubernetes.io/docs/tasks/access-kubernetes-api/custom-resources/custom-resource-definitions/)
  * Auto-generated from the new resource type
  * Adds a new resource kind/type to the kubernetes extension api
- [Controller](https://github.com/kubernetes/community/blob/master/contributors/devel/controllers.md)
  * Registers Handler functions to various CRUD events on instances of the new resource
  * Processes the work queue for applying resources changes to the cluster
  * Contains the Reconcile function which runs our application behaviours

## Compiling from source

In order to compile this project you need [Go 1.9 or newer](https://golang.org/dl), the [deps binary](https://github.com/golang/dep#installation) and [kubebuilder](https://github.com/kubernetes-sigs/kubebuilder) installed.

Fetch the source code:

```
go get gitlab.com/charts/components/gitlab-operator
cd $GOPATH/src/gitlab.com/charts/components/gitlab-operator
git checkout master
```

Build the project:

```
make
```

## Release and Version information

See the [Release documentation](docs/release.md)

## Updating Dependencies

See the [Updating Dependencies documentation](docs/updating-dependencies.md)

## Resources

- [Introducing the Operator Framework](https://www.redhat.com/en/blog/introducing-operator-framework-building-apps-kubernetes)
- [Custom Resources Concept](https://kubernetes.io/docs/concepts/extend-kubernetes/api-extension/custom-resources/)
- [Sample custom controller by trstringer](https://medium.com/@trstringer/create-kubernetes-controllers-for-core-and-custom-resources-62fc35ad64a3)
- [Operator Framework](https://github.com/operator-framework)
  * A group of open source projects meant for creating/managing operators.
- [Kubernetes Custom Controller - Core Resource Handling](https://github.com/trstringer/k8s-controller-core-resource)
  * The initial example this operator was built from
  * An example of a custom Kubernetes controller that's only purpose is to watch for the creation, updating, or deletion of all pods.
- [Kubernetes Sample Controller](https://github.com/kubernetes/sample-controller)
  * A simple controller for watching Foo resources as defined with a CustomResourceDefinition
- [Client Controller Docs](https://github.com/kubernetes/kubernetes/blob/master/staging/src/k8s.io/sample-controller/docs/controller-client-go.md)
- [Kubernetes Deep Dive: Code Generation for CustomResources](https://blog.openshift.com/kubernetes-deep-dive-code-generation-customresources/)
  * Information on generating the ClientSets, Listers, and Informers for your CRD api type
- [Prometheus Operator](https://github.com/coreos/prometheus-operator)
  * A real-world example of an operator
- [K8s Deployment Controller](https://github.com/kubernetes/kubernetes/blob/v1.9.9/pkg/controller/deployment/deployment_controller.go)
  * Kubernetes' native deployment controller, as an example of what can be done with a controller
- [Guide to Kubernetes Operator SDK](https://banzaicloud.com/blog/operator-sdk/)
- [KubeBuilder Docs](http://kubebuilder.netlify.com/)

## Attributions

- The code of gitlab-operator was initially built on the work of github user [@trstringer on sample kubernetes custom controller](https://github.com/trstringer/k8s-controller-core-resource)

- The initial project scaffolding was generated using [kubebuilder](https://github.com/kubernetes-sigs/kubebuilder):
  - `kubebuilder init --domain com --license apache2 --owner "GitLab"`
  - `kubebuilder create api --group gitlab --version v1beta1 --kind GitLab`
