/*
Copyright 2018 GitLab.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package gitlab

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"time"

	"github.com/ghodss/yaml"

	"github.com/blang/semver"
	gitlabv1beta1 "gitlab.com/charts/components/gitlab-operator/pkg/apis/gitlab/v1beta1"
	"gitlab.com/charts/components/gitlab-operator/pkg/version"
	appsv1 "k8s.io/api/apps/v1"
	batchv1 "k8s.io/api/batch/v1"
	"k8s.io/api/core/v1"
	rbacv1 "k8s.io/api/rbac/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/labels"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/types"
	"k8s.io/apimachinery/pkg/util/intstr"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/controller"
	"sigs.k8s.io/controller-runtime/pkg/event"
	"sigs.k8s.io/controller-runtime/pkg/handler"
	"sigs.k8s.io/controller-runtime/pkg/manager"
	"sigs.k8s.io/controller-runtime/pkg/predicate"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
	"sigs.k8s.io/controller-runtime/pkg/source"
)

const (
	sharedSecretsLabelSelector          = "app=shared-secrets"
	unicornLabelSelector                = "app=unicorn"
	gitlabShellLabelSelector            = "app=gitlab-shell"
	sidekiqLabelSelector                = "app=sidekiq"
	taskRunnerLabelSelector             = "app=task-runner"
	minioLabelSelector                  = "app=minio"
	nginxIngressControllerLabelSelector = "app=nginx-ingress"
	redisLabelSelector                  = "app=redis"
	registryLabelSelector               = "app=registry"
	gitalyLabelSelector                 = "app=gitaly"
	migrationsLabelSelector             = "app=migrations"
	gitlabVersionAnnotationKey          = "gitlab.com/version"
	gitlabLastRestartAnnotationKey      = "gitlab.com/last-restart"
	gitlabOperatorNameLabelKey          = "gitlab.com/operator-name"
	gitlabOperatorNameLabelValue        = "gitlab-operator"
	timeFormat                          = "20060102150405"
	defaultJobParallelism               = 1
	defaultDaemonSetMaxUnavailable      = 1
	helmLabelSelector                   = "release"
	gitalyRollingUpdateTimeout          = 300 * time.Second
	gitalyRollingUpdateCheckPeriod      = 5 * time.Second
)

// Add creates a new GitLab Controller and adds it to the Manager with default RBAC. The Manager will set fields on the Controller
// and Start it when the Manager is Started.
func Add(mgr manager.Manager) error {
	return add(mgr, newReconciler(mgr))
}

// newReconciler returns a new reconcile.Reconciler
func newReconciler(mgr manager.Manager) reconcile.Reconciler {
	return &ReconcileGitLab{Client: mgr.GetClient(), scheme: mgr.GetScheme()}
}

// add adds a new Controller to mgr with r as the reconcile.Reconciler
func add(mgr manager.Manager, r reconcile.Reconciler) error {
	// Create a new controller
	c, err := controller.New("gitlab-controller", mgr, controller.Options{Reconciler: r})
	if err != nil {
		return err
	}

	// Only process gitlab objects with the appropriate version label
	gitlabPredicate := predicate.Funcs{
		UpdateFunc: func(e event.UpdateEvent) bool {
			if versionLabel, _ := e.MetaNew.GetLabels()["controller.gitlab.com"]; versionLabel != version.GetVersion() {
				return false
			}
			return e.ObjectOld != e.ObjectNew
		},
		CreateFunc: func(e event.CreateEvent) bool {
			if versionLabel, _ := e.Meta.GetLabels()["controller.gitlab.com"]; versionLabel != version.GetVersion() {
				return false
			}
			return true
		},
		DeleteFunc: func(e event.DeleteEvent) bool {
			if versionLabel, _ := e.Meta.GetLabels()["controller.gitlab.com"]; versionLabel != version.GetVersion() {
				return false
			}
			return true
		},
		GenericFunc: func(e event.GenericEvent) bool {
			if versionLabel, _ := e.Meta.GetLabels()["controller.gitlab.com"]; versionLabel != version.GetVersion() {
				return false
			}
			return true
		},
	}

	// Watch for changes to GitLab
	err = c.Watch(&source.Kind{Type: &gitlabv1beta1.GitLab{}}, &handler.EnqueueRequestForObject{}, gitlabPredicate)
	if err != nil {
		return err
	}

	// TODO: watch for all managed resources (deployments, statefulsets etc.) and trigger requeue when any changes are observed

	return nil
}

var _ reconcile.Reconciler = &ReconcileGitLab{}

// ReconcileGitLab reconciles a GitLab object
type ReconcileGitLab struct {
	client.Client
	scheme *runtime.Scheme
}

// Reconcile reads that state of the cluster for a GitLab object and makes changes based on the state read
// and what is in the GitLab.Spec
// Automatically generate RBAC rules to allow the Controller to read and write Deployments, StatefulSets and Jobs
// +kubebuilder:rbac:groups=apps,resources=deployments;statefulsets;daemonsets,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=batch,resources=jobs,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=rbac.authorization.k8s.io,resources=roles;rolebindings;clusterroles;clusterrolebindings,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=gitlab.com,resources=gitlabs,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups="",resources=pods;configmaps;secrets;serviceaccounts,verbs=get;list;watch;create;update;patch;delete
func (r *ReconcileGitLab) Reconcile(request reconcile.Request) (reconcile.Result, error) {
	// Fetch the GitLab instance
	gitlab := &gitlabv1beta1.GitLab{}
	err := r.Get(context.TODO(), request.NamespacedName, gitlab)
	if err != nil {
		if errors.IsNotFound(err) {
			// Object not found, return.  Created objects are automatically garbage collected.
			// For additional cleanup logic use finalizers.
			return reconcile.Result{}, nil
		}
		// Error reading the object - requeue the request.
		return reconcile.Result{}, err
	}

	// Pause all workloads before unpausing the shared-secrets Job to allow the
	// operator to control the update if required
	log.Printf("Pausing all workloads")
	err = r.pauseWorkloads(gitlab, true)
	if err != nil {
		return reconcile.Result{}, err
	}

	// Get the version of GitLab currently deployed
	// This needs to be 'worked out' instead of read from a previous known good
	// as we don't know what could have happened in the interim
	// We continue when we don't know the version.
	// In dev we use `master` as version. We want to force a upgrade.
	log.Print("Calculating current version")
	version, err := r.getCurrentVersion(gitlab)
	if err != nil {
		log.Printf("warning: %s", err)
	}
	log.Printf("Calculated version: %s", version)
	log.Printf("Desired version: %s", gitlab.Spec.Version)

	// Check whether we need to perform an update by comparing the current
	// version to the desired version
	// When it errors, we will continue with a upgrade and force it
	update, err := r.updateRequired(gitlab, version)
	if err != nil {
		log.Printf("warning could not determine if upgrade is required: %s", err)
		update = true
	}

	if update {

		log.Printf("Updating")

		log.Print("Running shared-secrets")
		err = r.runSharedSecrets(gitlab)
		if err != nil {
			log.Printf("Error: %s", err)
			return reconcile.Result{}, err
		}

		log.Print("Running pre migrations")
		err = r.runMigrations(gitlab, "pre")
		if err != nil {
			log.Printf("Error: %s", err)
			return reconcile.Result{}, err
		}

		log.Print("Rolling Gitaly")
		err = r.rollingUpdateGitaly(gitlab)
		if err != nil {
			return reconcile.Result{}, err
		}

	} else {
		log.Printf("No update required")
	}

	log.Printf("Unpausing all workloads")
	err = r.pauseWorkloads(gitlab, false)
	if err != nil {
		return reconcile.Result{}, err
	}

	if update {

		// Make sure the deployments are rolled
		err = r.finishGitlabDeployment(gitlab)
		if err != nil {
			return reconcile.Result{}, err
		}

		log.Print("Running post migrations")
		err = r.runMigrations(gitlab, "post")
		if err != nil {
			return reconcile.Result{}, err
		}

		log.Print("Rolling gitlab")
		err = r.rollingUpdateGitLab(gitlab)
		if err != nil {
			return reconcile.Result{}, err
		}
	}

	log.Printf("End of reconcile run")

	return reconcile.Result{}, nil
}

// unpauseWorkloads unpauses all GitLab related deployments, jobs, statefulsets
// and daemonsets
func (r *ReconcileGitLab) pauseWorkloads(gitlab *gitlabv1beta1.GitLab, pause bool) error {

	for _, labelSelector := range []string{
		sidekiqLabelSelector,
		taskRunnerLabelSelector,
		unicornLabelSelector,
		minioLabelSelector,
		nginxIngressControllerLabelSelector,
		redisLabelSelector,
		registryLabelSelector} {
		err := r.pauseDeployments(gitlab, labelSelector, pause)
		if err != nil {
			return err
		}
	}

	for _, labelSelector := range []string{
		gitalyLabelSelector,
		redisLabelSelector} {
		err := r.pauseStatefulSets(gitlab, labelSelector, pause)
		if err != nil {
			return err
		}
	}

	for _, labelSelector := range []string{
		sharedSecretsLabelSelector,
		migrationsLabelSelector} {
		err := r.pauseJobs(gitlab, labelSelector, pause)
		if err != nil {
			return err
		}
	}

	for _, labelSelector := range []string{
		nginxIngressControllerLabelSelector} {
		err := r.pauseDaemonSets(gitlab, labelSelector, pause)
		if err != nil {
			return err
		}
	}

	return nil
}

// getCurrentVersion works out the version of GitLab currently deployed by
// reading the gitlab.com/version annotations from the unicorn deployment and
// corresponding pods and taking the minimum. This implementation will need to
// be improved in the future
func (r *ReconcileGitLab) getCurrentVersion(gitlab *gitlabv1beta1.GitLab) (version string, err error) {

	zeroSemanticVersion, err := semver.Make("0.0.0")
	if err != nil {
		return "", err
	}
	currentSemanticVersion := zeroSemanticVersion

	// Unicorn deployments
	query := fmt.Sprintf("%s, %s=%s", unicornLabelSelector, helmLabelSelector, gitlab.Spec.HelmRelease)
	selector, err := labels.Parse(query)
	if err != nil {
		return "", err
	}

	deployments := &appsv1.DeploymentList{}
	err = r.List(context.TODO(), &client.ListOptions{Namespace: gitlab.Namespace, LabelSelector: selector}, deployments)
	if err != nil {
		return "", err
	}

	for _, deployment := range deployments.Items {
		version := deployment.Annotations[gitlabVersionAnnotationKey]
		semanticVersion, err := semver.Make(version)
		if err != nil {
			return "", err
		}
		if currentSemanticVersion.Compare(zeroSemanticVersion) == 0 || currentSemanticVersion.Compare(semanticVersion) > 0 {
			currentSemanticVersion = semanticVersion
		}
	}

	// Unicorn pods
	pods := &v1.PodList{}
	err = r.List(context.TODO(), &client.ListOptions{Namespace: gitlab.Namespace, LabelSelector: selector}, pods)
	if err != nil {
		return "", err
	}

	for _, pod := range pods.Items {
		version := pod.Annotations[gitlabVersionAnnotationKey]
		semanticVersion, err := semver.Make(version)
		if err != nil {
			return "", err
		}
		if currentSemanticVersion.Compare(zeroSemanticVersion) == 0 || currentSemanticVersion.Compare(semanticVersion) > 0 {
			currentSemanticVersion = semanticVersion
		}
	}

	if currentSemanticVersion.Compare(zeroSemanticVersion) == 0 {
		return "", fmt.Errorf("unable to determine current version")
	}

	return currentSemanticVersion.String(), nil
}

// updateRequired returns whether an update is required by comparing the
// current version to the desired version
func (r *ReconcileGitLab) updateRequired(gitlab *gitlabv1beta1.GitLab, currentVersion string) (update bool, err error) {

	gitlabSemanticVersion, err := semver.Make(gitlab.Spec.Version)
	if err != nil {
		return false, err
	}

	currentSemanticVersion, err := semver.Make(currentVersion)
	if err != nil {
		return false, err
	}

	comparison := gitlabSemanticVersion.Compare(currentSemanticVersion)

	if comparison == 0 {
		// Desired version and current version is the same
		// No update required
		return false, nil
	}

	if comparison > 0 {
		// Desired version is higher than current version
		// We need to run an update
		return true, nil
	}

	return false, fmt.Errorf("current version is higher than desired version")
}

func (r *ReconcileGitLab) rollingUpdateGitLab(gitlab *gitlabv1beta1.GitLab) error {

	// Sidekiq
	query := fmt.Sprintf("%s, %s=%s", sidekiqLabelSelector, helmLabelSelector, gitlab.Spec.HelmRelease)
	selector, err := labels.Parse(query)
	if err != nil {
		return err
	}
	deployments := &appsv1.DeploymentList{}
	err = r.List(context.TODO(), &client.ListOptions{Namespace: gitlab.Namespace, LabelSelector: selector}, deployments)
	if err != nil {
		return err
	}
	for _, deployment := range deployments.Items {
		deployment.Spec.Template.ObjectMeta.Annotations[gitlabLastRestartAnnotationKey] = time.Now().Format(timeFormat)
		err = r.Update(context.TODO(), &deployment)
		if err != nil {
			return err
		}
	}

	// Unicorn
	query = fmt.Sprintf("%s, %s=%s", unicornLabelSelector, helmLabelSelector, gitlab.Spec.HelmRelease)
	selector, err = labels.Parse(query)
	if err != nil {
		return err
	}
	err = r.List(context.TODO(), &client.ListOptions{Namespace: gitlab.Namespace, LabelSelector: selector}, deployments)
	if err != nil {
		return err
	}
	for _, deployment := range deployments.Items {
		deployment.Spec.Template.ObjectMeta.Annotations[gitlabLastRestartAnnotationKey] = time.Now().Format(timeFormat)
		err = r.Update(context.TODO(), &deployment)
		if err != nil {
			return err
		}
	}

	// Make sure the deployments are rolled
	err = r.finishGitlabDeployment(gitlab)
	if err != nil {
		return err
	}

	return nil
}

func (r *ReconcileGitLab) cleanUpJob(gitlab *gitlabv1beta1.GitLab, jobName string) error {
	query := fmt.Sprintf("job-name=%s", jobName)
	selector, err := labels.Parse(query)

	if err != nil {
		return err
	}

	job := &batchv1.Job{}
	err = r.Get(context.TODO(), types.NamespacedName{Name: jobName, Namespace: gitlab.Namespace}, job)
	if err != nil {
		return err
	}
	err = r.Delete(context.TODO(), job)
	if err != nil {
		return err
	}
	// delete pods

	pods := &v1.PodList{}
	err = r.List(context.TODO(), &client.ListOptions{Namespace: gitlab.Namespace, LabelSelector: selector}, pods)

	if err != nil {
		return err
	}

	for _, pod := range pods.Items {
		err = r.Delete(context.TODO(), &pod)
		if err != nil {
			return err
		}
	}

	return nil
}

func (r *ReconcileGitLab) cleanUpMigrations(gitlab *gitlabv1beta1.GitLab, jobName string) error {
	return r.cleanUpJob(gitlab, jobName)
}

// TODO: Requires major clean up
func (r *ReconcileGitLab) cleanUpSharedSecrets(gitlab *gitlabv1beta1.GitLab, sharedSecretsJobName, selfSignedCertJobName string) error {
	rbacName := fmt.Sprintf("%s-%s", gitlab.Spec.HelmRelease, "shared-secrets")

	err := r.cleanUpJob(gitlab, sharedSecretsJobName)
	if err != nil {
		return err
	}

	if selfSignedCertJobName != "" {
		err = r.cleanUpJob(gitlab, selfSignedCertJobName)
	}

	serviceAccount := &v1.ServiceAccount{}
	role := &rbacv1.Role{}
	roleBinding := &rbacv1.RoleBinding{}

	err = r.Get(context.TODO(), types.NamespacedName{Name: rbacName, Namespace: gitlab.Namespace}, serviceAccount)
	if err != nil {
		return err
	}

	err = r.Delete(context.TODO(), serviceAccount)
	if err != nil {
		return err
	}

	err = r.Get(context.TODO(), types.NamespacedName{Name: rbacName, Namespace: gitlab.Namespace}, role)
	if err != nil {
		return err
	}

	err = r.Delete(context.TODO(), role)
	if err != nil {
		return err
	}

	err = r.Get(context.TODO(), types.NamespacedName{Name: rbacName, Namespace: gitlab.Namespace}, roleBinding)
	if err != nil {
		return err
	}

	err = r.Delete(context.TODO(), roleBinding)
	if err != nil {
		return err
	}

	return nil
}

func (r *ReconcileGitLab) runMigrations(gitlab *gitlabv1beta1.GitLab, migType string) error {

	migrationsJobName := fmt.Sprintf("%s.%s.%s.%s", "migrations", gitlab.Spec.Version, migType, time.Now().Format(timeFormat))
	migrationsJobName = fmt.Sprintf("%s.%s", truncate(gitlab.Spec.HelmRelease, 63-len(migrationsJobName)-1), migrationsJobName)

	configMap := &v1.ConfigMap{}

	err := r.Get(context.TODO(), types.NamespacedName{Name: gitlab.Spec.Templates.MigrationsTemplate.ConfigMapName, Namespace: gitlab.Namespace}, configMap)

	if err != nil {
		return err
	}

	job := &batchv1.Job{}
	jsonData, err := yaml.YAMLToJSON([]byte(configMap.Data[gitlab.Spec.Templates.MigrationsTemplate.ConfigMapKey]))

	if err != nil {
		return err
	}

	err = json.Unmarshal(jsonData, job)

	if err != nil {
		return err
	}

	migrationsJob := formatMigrationsJob(gitlab, job, migrationsJobName, migType)

	err = r.Create(context.TODO(), migrationsJob)
	if err != nil {
		return err
	}

	// Now we should wait for the job to finish excution!
	timeout := 0
	for {
		if timeout > 300 {
			return fmt.Errorf("%s migrations Job didn't finish in 5 minutes. Backing off", migType)
		}

		job := &batchv1.Job{}
		err := r.Get(context.TODO(), types.NamespacedName{Name: migrationsJobName, Namespace: gitlab.Namespace}, job)

		if err != nil {
			continue
		}

		if job.Status.Succeeded == 1 {
			break
		}

		if job.Status.Failed == 1 {
			return fmt.Errorf("Migration job %s failed", migrationsJobName)
		}

		timeout += 5
		time.Sleep(5 * time.Second)
	}

	err = r.cleanUpMigrations(gitlab, migrationsJobName)
	if err != nil {
		log.Printf("Warning: Can not delete migrations job %s ", err)
	}

	log.Print("Migrations completed")
	return nil
}

// TODO: requires major refactoring and cleanup specifically in the way the rbac is handled
// reflections can probably be used here to DRY up stuff. Also parsing can become a single json.UnMarshal
func (r *ReconcileGitLab) runSharedSecrets(gitlab *gitlabv1beta1.GitLab) error {

	sharedSecretsJobName := fmt.Sprintf("%s.%s.%s", "shared-secrets", gitlab.Spec.Version, time.Now().Format(timeFormat))
	sharedSecretsJobName = fmt.Sprintf("%s.%s", truncate(gitlab.Spec.HelmRelease, 63-len(sharedSecretsJobName)-1), sharedSecretsJobName)

	selfSignedCertJobName := fmt.Sprintf("%s.%s.%s", "self-signed-certs", gitlab.Spec.Version, time.Now().Format(timeFormat))
	selfSignedCertJobName = fmt.Sprintf("%s.%s", truncate(gitlab.Spec.HelmRelease, 63-len(selfSignedCertJobName)-1), selfSignedCertJobName)

	configMap := &v1.ConfigMap{}

	err := r.Get(context.TODO(), types.NamespacedName{Name: gitlab.Spec.Templates.SharedSecretsTemplate.ConfigMapName, Namespace: gitlab.Namespace}, configMap)

	if err != nil {
		return err
	}

	// We need to create rbac dependencies

	serviceAccount := &v1.ServiceAccount{}
	role := &rbacv1.Role{}
	roleBinding := &rbacv1.RoleBinding{}
	job := &batchv1.Job{}

	var selfSignedJsonData []byte
	selfSignedJob := &batchv1.Job{}

	if configMap.Data["selfSignedCertTemplate"] != "" {
		selfSignedJsonData, err = yaml.YAMLToJSON([]byte(configMap.Data["selfSignedCertTemplate"]))
		if err != nil {
			return err
		}
		err = json.Unmarshal(selfSignedJsonData, selfSignedCertJobName)
		if err != nil {
			return err
		}
	} else {
		selfSignedCertJobName = ""
	}

	serviceAccountJsonData, err := yaml.YAMLToJSON([]byte(configMap.Data[gitlab.Spec.Templates.SharedSecretsTemplate.ServiceAccountKey]))
	if err != nil {
		return err
	}

	roleJsonData, err := yaml.YAMLToJSON([]byte(configMap.Data[gitlab.Spec.Templates.SharedSecretsTemplate.RoleKey]))
	if err != nil {
		return err
	}

	roleBindingJsonData, err := yaml.YAMLToJSON([]byte(configMap.Data[gitlab.Spec.Templates.SharedSecretsTemplate.RoleBindingKey]))
	if err != nil {
		return err
	}

	jobJsonData, err := yaml.YAMLToJSON([]byte(configMap.Data[gitlab.Spec.Templates.SharedSecretsTemplate.ConfigMapKey]))
	if err != nil {
		return err
	}

	err = json.Unmarshal(serviceAccountJsonData, serviceAccount)
	if err != nil {
		return err
	}
	serviceAccount.ObjectMeta.Namespace = gitlab.Namespace

	err = json.Unmarshal(roleJsonData, role)
	if err != nil {
		return err
	}
	role.ObjectMeta.Namespace = gitlab.Namespace

	err = json.Unmarshal(roleBindingJsonData, roleBinding)
	if err != nil {
		return err
	}
	roleBinding.ObjectMeta.Namespace = gitlab.Namespace

	err = json.Unmarshal(jobJsonData, job)
	if err != nil {
		return err
	}

	err = r.Get(context.TODO(), types.NamespacedName{Name: serviceAccount.ObjectMeta.Name, Namespace: gitlab.Namespace}, &v1.ServiceAccount{})
	if err != nil {
		err = r.Create(context.TODO(), serviceAccount)
		if err != nil {
			return err
		}
	}

	err = r.Get(context.TODO(), types.NamespacedName{Name: role.ObjectMeta.Name, Namespace: gitlab.Namespace}, &rbacv1.Role{})
	if err != nil {
		err = r.Create(context.TODO(), role)
		if err != nil {
			return err
		}
	}

	err = r.Get(context.TODO(), types.NamespacedName{Name: roleBinding.ObjectMeta.Name, Namespace: gitlab.Namespace}, &rbacv1.RoleBinding{})
	if err != nil {
		err = r.Create(context.TODO(), roleBinding)
		if err != nil {
			return err
		}
	}

	sharedSecretsJob := formatSharedSecretsJob(gitlab, job, sharedSecretsJobName)

	err = r.Create(context.TODO(), sharedSecretsJob)
	if err != nil {
		return err
	}

	if configMap.Data["selfSignedCertTemplate"] != "" {
		selfSignedJob = formatSharedSecretsJob(gitlab, selfSignedJob, selfSignedCertJobName)

		err = r.Create(context.TODO(), selfSignedJob)
		if err != nil {
			return err
		}
	}

	// Now we should wait for the job to finish excution!
	timeout := 0
	for {
		if timeout > 300 {
			return fmt.Errorf("shared-secrets Job didn't finish in 5 minutes. Backing off")
		}

		job := &batchv1.Job{}
		err := r.Get(context.TODO(), types.NamespacedName{Name: sharedSecretsJobName, Namespace: gitlab.Namespace}, job)

		if err != nil {
			continue
		}

		if job.Status.Succeeded == 1 {
			break
		}

		if job.Status.Failed == 1 {
			return fmt.Errorf("Shared secrets job %s failed", sharedSecretsJobName)
		}

		timeout += 5
		time.Sleep(5 * time.Second)
	}

	err = r.cleanUpSharedSecrets(gitlab, sharedSecretsJobName, selfSignedCertJobName)
	if err != nil {
		log.Printf("Warning: Can not delete shared secrets %s ", err)
	}

	log.Print("Shared secrets job completed")
	return nil
}

func (r *ReconcileGitLab) pauseJobs(gitlab *gitlabv1beta1.GitLab, labelSelector string, pause bool) error {

	query := fmt.Sprintf("%s, %s=%s", labelSelector, helmLabelSelector, gitlab.Spec.HelmRelease)
	selector, err := labels.Parse(query)
	if err != nil {
		return err
	}

	jobs := &batchv1.JobList{}
	err = r.List(context.TODO(), &client.ListOptions{Namespace: gitlab.Namespace, LabelSelector: selector}, jobs)
	if err != nil {
		return err
	}

	parallelism := int32Pointer(defaultJobParallelism)
	if pause {
		parallelism = int32Pointer(0)
	}

	for _, job := range jobs.Items {
		job.Spec.Parallelism = parallelism

		err = r.Update(context.TODO(), &job)
		if err != nil {
			return err
		}
	}

	return nil
}

func (r *ReconcileGitLab) pauseDeployments(gitlab *gitlabv1beta1.GitLab, labelSelector string, pause bool) error {

	query := fmt.Sprintf("%s, %s=%s", labelSelector, helmLabelSelector, gitlab.Spec.HelmRelease)
	selector, err := labels.Parse(query)
	if err != nil {
		return err
	}

	deployments := &appsv1.DeploymentList{}
	err = r.List(context.TODO(), &client.ListOptions{Namespace: gitlab.Namespace, LabelSelector: selector}, deployments)
	if err != nil {
		return err
	}

	specPaused := false
	if pause {
		specPaused = true
	}

	for _, deployment := range deployments.Items {
		deployment.Spec.Paused = specPaused

		err = r.Update(context.TODO(), &deployment)
		if err != nil {
			return err
		}
	}

	return nil
}

func (r *ReconcileGitLab) pauseStatefulSets(gitlab *gitlabv1beta1.GitLab, labelSelector string, pause bool) error {

	query := fmt.Sprintf("%s, %s=%s", labelSelector, helmLabelSelector, gitlab.Spec.HelmRelease)
	selector, err := labels.Parse(query)
	if err != nil {
		return err
	}

	statefulSets := &appsv1.StatefulSetList{}
	err = r.List(context.TODO(), &client.ListOptions{Namespace: gitlab.Namespace, LabelSelector: selector}, statefulSets)
	if err != nil {
		return err
	}

	for _, statefulSet := range statefulSets.Items {
		if pause {
			statefulSet.Spec.UpdateStrategy.RollingUpdate.Partition = statefulSet.Spec.Replicas
		} else {
			statefulSet.Spec.UpdateStrategy.RollingUpdate.Partition = int32Pointer(0)
		}
		err = r.Update(context.TODO(), &statefulSet)
		if err != nil {
			return err
		}
	}

	return nil
}

func (r *ReconcileGitLab) pauseDaemonSets(gitlab *gitlabv1beta1.GitLab, labelSelector string, pause bool) error {

	query := fmt.Sprintf("%s, %s=%s", labelSelector, helmLabelSelector, gitlab.Spec.HelmRelease)
	selector, err := labels.Parse(query)
	if err != nil {
		return err
	}

	daemonSets := &appsv1.DaemonSetList{}
	err = r.List(context.TODO(), &client.ListOptions{Namespace: gitlab.Namespace, LabelSelector: selector}, daemonSets)
	if err != nil {
		return err
	}

	maxUnavailable := &intstr.IntOrString{IntVal: defaultDaemonSetMaxUnavailable}
	if pause {
		maxUnavailable = &intstr.IntOrString{IntVal: 0}
	}

	for _, daemonSet := range daemonSets.Items {
		daemonSet.Spec.UpdateStrategy.RollingUpdate.MaxUnavailable = maxUnavailable

		err = r.Update(context.TODO(), &daemonSet)
		if err != nil {
			return err
		}
	}

	return nil
}

func (r *ReconcileGitLab) checkStatefulSetFinished(gitlab *gitlabv1beta1.GitLab, labelSelector string) (bool, error) {
	query := fmt.Sprintf("%s, %s=%s", labelSelector, helmLabelSelector, gitlab.Spec.HelmRelease)
	selector, err := labels.Parse(query)
	if err != nil {
		return false, err
	}

	statefulSets := &appsv1.StatefulSetList{}
	err = r.List(context.TODO(), &client.ListOptions{Namespace: gitlab.Namespace, LabelSelector: selector}, statefulSets)
	if err != nil {
		return false, err
	}
	for _, statefulSet := range statefulSets.Items {
		observedGeneration := statefulSet.Generation
		lastObservedGeneration := statefulSet.Status.ObservedGeneration
		if observedGeneration > lastObservedGeneration {
			log.Printf("StatefulSet is still updating for %s", statefulSet.Name)
			return false, nil
		}
		log.Printf("StatefulSet is updated for %s", statefulSet.Name)
	}
	return true, nil
}

func (r *ReconcileGitLab) checkDeploymentFinished(gitlab *gitlabv1beta1.GitLab, labelSelector string) (bool, error) {
	query := fmt.Sprintf("%s, %s=%s", labelSelector, helmLabelSelector, gitlab.Spec.HelmRelease)
	selector, err := labels.Parse(query)
	if err != nil {
		return false, err
	}

	deployments := &appsv1.DeploymentList{}
	err = r.List(context.TODO(), &client.ListOptions{Namespace: gitlab.Namespace, LabelSelector: selector}, deployments)
	if err != nil {
		return false, err
	}
	for _, deployment := range deployments.Items {
		observedGeneration := deployment.Generation
		lastObservedGeneration := deployment.Status.ObservedGeneration
		if observedGeneration > lastObservedGeneration {
			log.Printf("Deployment is still updating for %s", deployment.Name)
			return false, nil
		}
		log.Printf("Deployment is updated for %s", deployment.Name)
	}

	return true, nil
}

func (r *ReconcileGitLab) finishGitlabDeployment(gitlab *gitlabv1beta1.GitLab) error {
	// We have to wait for some time, as it is possible that update call is a bit delayed.
	log.Printf("Waiting for 5 seconds to start checking for status of Gitlab deployments")
	time.Sleep(5 * time.Second)

	timeout := 0
	for {
		if timeout > 300 {
			return fmt.Errorf("timeout was longer then 300")
		}
		deployed := false
		// Sidekiq
		deployed, err := r.checkDeploymentFinished(gitlab, sidekiqLabelSelector)
		if err != nil {
			log.Printf("Ran into error but will try to continue: %v", err)
			continue
		}

		// Unicorn
		deployed, err = r.checkDeploymentFinished(gitlab, unicornLabelSelector)

		if err != nil {
			log.Printf("Ran into error but will try to continue: %v", err)
			continue
		}

		if deployed {
			log.Printf("Sidekiq and Unicorn are rolled")
			break
		} else {
			timeout += 5
			time.Sleep(5 * time.Second)
		}
	}

	return nil
}

func int32Pointer(x int32) *int32 {
	return &x
}

func int64Pointer(x int64) *int64 {
	return &x
}

func (r *ReconcileGitLab) rollingUpdateGitaly(gitlab *gitlabv1beta1.GitLab) error {

	// Unpause Gitaly
	err := r.pauseStatefulSets(gitlab, gitalyLabelSelector, false)
	if err != nil {
		return err
	}

	// Wait for Gitaly to finish rolling update
	finishedChannel := make(chan error, 0)
	go func() {
		for {
			deployed, err := r.checkStatefulSetFinished(gitlab, gitalyLabelSelector)
			if err != nil {
				finishedChannel <- err
				return
			}
			if deployed != true {
				time.Sleep(gitalyRollingUpdateCheckPeriod)
				continue
			}
			finishedChannel <- nil
		}
	}()

	select {
	case err := <-finishedChannel:
		if err != nil {
			return err
		}
		log.Printf("Gitaly is rolled")
	case <-time.After(gitalyRollingUpdateTimeout):
		return fmt.Errorf("timeout waiting for Gitaly to roll")
	}

	return nil
}

func truncate(str string, n int) string {
	if len(str) > n {
		return str[:n]
	}

	return str
}
