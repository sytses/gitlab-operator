/*
Copyright 2018 GitLab.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package v1beta1

import (
	"os"
	"testing"

	"github.com/onsi/gomega"
)

const APISubgroupEnv string = "API_SUBGROUP"

func TestGetGroupName(t *testing.T) {
	g := gomega.NewGomegaWithT(t)

	saveEnvVar := os.Getenv(APISubgroupEnv)
	// Clean up the test fixture and restore the environment variable
	defer func() {
		os.Setenv(APISubgroupEnv, saveEnvVar)
		// Should warn user if couldn't restore the environment
	}()

	// Clear any existing environment variable and test the default behavior
	err := os.Unsetenv(APISubgroupEnv)
	g.Expect(err).NotTo(gomega.HaveOccurred())
	g.Expect(getGroupName()).To(gomega.Equal("gitlab.com"))

	// Update the environment variable and test the non-default behavior
	err = os.Setenv(APISubgroupEnv, "foo")
	g.Expect(err).NotTo(gomega.HaveOccurred())
	g.Expect(getGroupName()).To(gomega.Equal("foo.gitlab.com"))

}
